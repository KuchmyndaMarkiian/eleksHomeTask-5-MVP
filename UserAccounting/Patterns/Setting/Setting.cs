﻿using System;

namespace UserAccounting.Patterns.Setting
{
    sealed class Setting
    {
        private static Lazy<Setting> instance=new Lazy<Setting>(()=>new Setting());
        public string FilePath { get; set; } = null;
        private Setting() { }
        public static Setting GetInstance() => instance.Value;
    }
}
