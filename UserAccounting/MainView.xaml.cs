using System;
using System.Windows;
using System.Windows.Controls;
using UserAccounting.Model;
using UserAccounting.MVP.Presenter;
using UserAccounting.MVP.View;
using UserAccounting.UI_messages;
using Xceed.Wpf.Toolkit;
using RichTextBox = Xceed.Wpf.Toolkit.RichTextBox;

namespace UserAccounting
{
    /// <summary>
    /// ������ �������������� ��� MainWindow.xaml
    /// </summary>
    public partial class MainView : Window, IView
    {
        private IPresenter _presenter;

        public MainView()
        {
            _presenter = new UserPresenter(this);
            Resources["Users"] = _presenter.UserList;

            InitializeComponent();
            Name = TName;
            Surname = TSurname;
            Age = TAge;
            Job = TJob;
            Logger = TLogger;
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string tmp = TName.Text;
                string name = string.IsNullOrEmpty(tmp) ? (String) TName.Watermark : tmp;
                tmp = TSurname.Text;
                string surname = string.IsNullOrEmpty(tmp) ? (String) TSurname.Watermark : tmp;
                tmp = TJob.Text;
                string job = string.IsNullOrEmpty(tmp) ? (String) TJob.Watermark : tmp;
                
                if (!_presenter.IsAdded(new User {Name = name, Surname = surname, Age = TAge.Value.Value, Job = job}))
                {
                    GuiInteraction.MessageBox(this, "�������", "���������� �� �������");
                }
            }
            catch(Exception exception)
            {
                GuiInteraction.MessageBox(this, "�������", exception.Message);
            }
        }
        private void Update_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string tmp = TName.Text;
                string name = string.IsNullOrEmpty(tmp) ? (String)TName.Watermark : tmp;
                tmp = TSurname.Text;
                string surname = string.IsNullOrEmpty(tmp) ? (String)TSurname.Watermark : tmp;
                tmp = TJob.Text;
                string job = string.IsNullOrEmpty(tmp) ? (String)TJob.Watermark : tmp;
                if (!_presenter.IsUpdated(new User { Name = name, Surname = surname, Age = TAge.Value.Value, Job = job }))
                {
                    GuiInteraction.MessageBox(this, "�������", "���������� �� ���������");
                }
            }
            catch
            {
                GuiInteraction.MessageBox(this, "�������", "������� ���������� �����");
            }
            Resources["Users"] = _presenter.UserList;
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (_presenter.IsDeleted(ListBox.SelectedIndex))
            {
                GuiInteraction.MessageBox(this, "�������", "���������� �� ���������");
            }
        }

        public RichTextBox Logger { get; set; }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            _presenter.Save();
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            _presenter.Open();
        }

        private void ListBox_OnSelected(object sender, RoutedEventArgs e)
        {
            _presenter.UpdateView(ListBox.SelectedIndex);
        }

        public TextBox Name { get; set; }
        public TextBox Surname { get; set; }
        public IntegerUpDown Age { get; set; }
        public TextBox Job { get; set; }

       
    }
}
