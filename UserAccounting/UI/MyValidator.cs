﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UserAccounting.Model;
using UserAccounting.MVP.View;
using UserAccounting.UI_messages;

namespace UserAccounting.UI
{
    static class MyValidator
    {
        public static bool ValidateUser(User user,IView view)
        {
            var context = new ValidationContext(user);
            var results = new List<ValidationResult>();
            StringBuilder builder = new StringBuilder();
            if (!Validator.TryValidateObject(user, context, results,true))
            {

                foreach (var validationResult in results)
                {
                    Append(validationResult.ErrorMessage,builder);
                }

                GuiInteraction.MessageBox((Window)view,"Помилка", builder.ToString());

                return false;
            }
            return true;
        }
        private static void Append(string text, StringBuilder builder)
        {
            Append(text);

            void Append(string text1) => builder.Append(text1);
        }
    }
}
