﻿using System.Windows;
using Microsoft.Win32;

namespace UserAccounting.UI_messages
{
    static class GuiInteraction
    {
        public static void MessageBox(Window window, string text, string title)
        {
            System.Windows.MessageBox.Show(window, title, text, MessageBoxButton.OK, MessageBoxImage.Information,
                MessageBoxResult.OK, MessageBoxOptions.None);
        }

        public static string OpenFileDialogPath()
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            var showDialog = dialog.ShowDialog();
            if (showDialog != null && !showDialog.Value)
            {
                return null;
            }

            return dialog.FileName;
        }

        public static string SaveFileDialogPath()
        {
            SaveFileDialog dialog = new SaveFileDialog();
            var showDialog = dialog.ShowDialog();
            if (showDialog != null && !showDialog.Value)
            {
                return null;
            }

            return dialog.FileName.Replace(".csv" ,"")+ ".csv";
        }
    }
}
