﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAccounting.MVP.Model;

namespace UserAccounting.Injector
{
    class Injector:IInjector
    {
        private UserList UserList;

        public Injector(UserList list)
        {
            UserList = list;
        }
        public UserList GetList()
        {
            return UserList;
        }
    }
}
