﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleInjector;
using UserAccounting.MVP.Model;
using UserAccounting.Serializer;

namespace UserAccounting.Injector
{
    static class MyContainer
    {
        private static Container Container;

        public static void Run()
        {
            Container = new Container();
            Container.Register<ICollection,UserList>();
            Container.Register<IInjector, Injector>();
            Container.Register<ISerializer, CsvSerializer>();
            Container.Verify();
        }

        public static UserList GetList()
        {
            return Container.GetInstance<IInjector>().GetList();
        }
        public static ISerializer GetSerializer()
        {
            return Container.GetInstance<ISerializer>();
        }
    }
}
