﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserAccounting.MVP.Model;

namespace UserAccounting.Injector
{
    interface IInjector
    {
        UserList GetList();
    }
}
