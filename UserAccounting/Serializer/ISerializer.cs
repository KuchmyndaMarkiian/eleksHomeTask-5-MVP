﻿namespace UserAccounting.Serializer
{
    interface ISerializer
    {
        string Serialize<T>(T value);
    }
}
