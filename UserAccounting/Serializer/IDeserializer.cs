﻿namespace UserAccounting.Serializer
{
    interface IDeserializer
    {
        T Deserialize<T>(string value);
    }
}
