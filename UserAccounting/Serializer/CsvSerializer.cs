﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace UserAccounting.Serializer
{
    class CsvSerializer : ISerializer
    {
        public string Serialize<T>(T value)
        {
            var type = typeof(T);
            StringBuilder builder = new StringBuilder();
            for (var index = 0; index < type.GetProperties().Length; index++)
            {
                var property = type.GetProperties()[index];
                Append($"{property.GetValue(value, null)}");
                Append(index == type.GetProperties().Length - 1 ? "\n" : ",");
            }
            return builder.ToString();

           
            void Append(string text) => builder.Append(text);
        }
    }
}

