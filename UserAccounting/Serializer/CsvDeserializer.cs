﻿using System;
using System.Linq;
using UserAccounting.Model;

namespace UserAccounting.Serializer
{
    class CsvDeserializer : IDeserializer
    {
        //тут не знаю як це зреалізувати через рефлексію
        public T Deserialize<T>(string line)
        {
            var type = typeof(T);
            var constructor = type.GetConstructor(new Type[] {});
            if (constructor == null)
            {
                object res = null;
                return (T) res;
            }
            var obj = constructor.Invoke(new object[] {});
            var properties = type.GetProperties();

            var lexems = line.Split(',').ToArray();
            if (lexems.Length == properties.Length)
            {
                if (type.Name != nameof(User))
                {
                    return (T) obj;
                }
                var user = new User
                {
                    Name = lexems[0],
                    Surname = lexems[1],
                    Age = int.Parse(lexems[2]),
                    Job = lexems.Last()
                };
                obj = user;
            }
            return (T) obj;
        }
    }
}

