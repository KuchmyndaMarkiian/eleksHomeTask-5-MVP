﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;


namespace UserAccounting.Model
{
   
    class User:INotifyPropertyChanged
    {
        [RegularExpression("^[a-zA-Z0-9 ]*$", ErrorMessage = "Без спецсимволів! ")]
        [Required]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "Задовге поле. ")]
        public string Name { get; set; }
        [RegularExpression("^[a-zA-Z0-9 ]*$", ErrorMessage = "Без спецсимволів! ")]
        [Required]
        [StringLength(100, MinimumLength = 1, ErrorMessage = "Задовге поле. ")]
        public string Surname { get; set; }
        [Required]
        [Range(0,300)]
        public int Age { get; set; }
        [Required]
        [StringLength(255, MinimumLength = 1, ErrorMessage = "Задовге поле. ")]
        public string Job { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
