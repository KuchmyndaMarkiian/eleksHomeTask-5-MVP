﻿using System.Windows.Controls;
using Xceed.Wpf.Toolkit;

namespace UserAccounting.MVP.View
{
    interface IView
    {
        TextBox Name { get; set; }
        TextBox Surname { get; set; }
        IntegerUpDown Age { get; set; }
        TextBox Job { get; set; }
        Xceed.Wpf.Toolkit.RichTextBox Logger { get; set; }
    }
}
