﻿using UserAccounting.Model;
using UserAccounting.MVP.Model;
using UserAccounting.MVP.View;

namespace UserAccounting.MVP.Presenter
{
    interface IPresenter
    {
        IView View { get; set; }
        User CurrentUser { get; set; }
        void Save();
        void Open();
        bool IsAdded(User user);
        bool IsUpdated(User user);
        bool IsDeleted(int index);
        void UpdateView(int index);
        void Logging(string s);
        User GetModel();
        UserList UserList { get; set; }
    }
}
