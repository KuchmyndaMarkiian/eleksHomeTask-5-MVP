﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using UserAccounting.Editors;
using UserAccounting.Injector;
using UserAccounting.Model;
using UserAccounting.MVP.Model;
using UserAccounting.MVP.View;
using UserAccounting.Patterns.Setting;
using UserAccounting.Serializer;
using UserAccounting.UI;
using UserAccounting.UI_messages;

namespace UserAccounting.MVP.Presenter
{
    class UserPresenter : IPresenter
    {
        public IView View { get; set; }
        public User CurrentUser { get; set; }

        public UserPresenter(IView view)
        {
            CurrentUser = null;
            View = view;

            //IoC
            MyContainer.Run();
            UserList = MyContainer.GetList();
        }

        public void Save()
        {
            var path = Setting.GetInstance().FilePath;
            if (path == null)
            {
                path = GuiInteraction.SaveFileDialogPath();
                if (path == null)
                {
                    return;
                }
            }
            CsvEditor csvEditor = new CsvEditor(path);
            ISerializer csvSerializer = MyContainer.GetSerializer();
            csvEditor.Write(UserList.Select(u => csvSerializer.Serialize(u)).ToArray());
            Logging($"Збережено у файлі: {path}");
        }

        public void Open()
        {
            var path = GuiInteraction.OpenFileDialogPath();
            if (path == null)
            {
                return;
            }
            Setting.GetInstance().FilePath = path;
            CsvEditor csvEditor = new CsvEditor(path);
            CsvDeserializer csvDeserializer = new CsvDeserializer();
            var list = csvEditor.Read();
            UserList.Clear();
            foreach (var line in list)
            {
                UserList.Add(csvDeserializer.Deserialize<User>(line));
            }

            Logging($"Відкрито файл: {path}");
        }

        public bool IsUpdated(User user)
        {
            try
            {
                if (!MyValidator.ValidateUser(user, View))
                {
                    throw new Exception("Require some property of user!");
                }
                Logging(
                    $"оновлено користувача ({CurrentUser.Name} {CurrentUser.Surname} вік:{CurrentUser.Age} робота:{CurrentUser.Job} " +
                    $"-> {user.Name} {user.Surname} вік:{user.Age} робота:{user.Job}");
                CurrentUser = user;
            }
            catch
            {
                return false;
            }
            return true;
        }

        public void Logging(string log)
        {
            var data = DateTime.Now;
            View.Logger.AppendText($"{data.Hour}:{data.Minute}:{data.Second} " + log + "\n");
        }

        public User GetModel() => CurrentUser;
        public UserList UserList { get; set; }

        public bool IsAdded(User user)
        {
            try
            {

                if (!MyValidator.ValidateUser(user, View))
                {
                    throw new Exception("Require some property of user!");
                }

                UserList.Add(user);
                Logging($"додано користувача {user.Name} {user.Surname}");

                CurrentUser = user;
            }
            catch
            {
                return false;
            }
            return true;
        }

        public void UpdateView(int index)
        {
            CurrentUser = UserList[index];

            View.Name.Text = CurrentUser.Name;
            View.Surname.Text = CurrentUser.Surname;
            View.Age.Value = CurrentUser.Age;
            View.Job.Text = CurrentUser.Job;
        }

        public bool IsDeleted(int index)
        {
            try
            {
                var user = UserList[index];
                Logging($"видалено користувача {user.Name} {user.Surname}");
                UserList.RemoveAt(index);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
