﻿using System.Collections.Generic;
using System.IO;

namespace UserAccounting.Editors
{
    class CsvEditor
    {
        private string path;

        public CsvEditor(string name)
        {
            path = name;

        }

        public void Write(string[] lines)
        {
            File.Delete(path);
            using (var FileStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
            {
                using (StreamWriter writer = new StreamWriter(FileStream))
                {

                    foreach (var str in lines)
                    {
                        writer.Write(str);
                    }
                    writer.Close();
                }
                FileStream.Close();
            }

        }

        public List<string> Read()
        {
            var list = new List<string>();
            using (var FileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (StreamReader reader = new StreamReader(FileStream))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        list.Add(line);
                    }
                    reader.Close();
                }
                FileStream.Close();
            }
            return list;
        }

    }
}
